# Stocks

## Problem

Investors will oftentimes publish their portfolio in a format like the below:

```
$ADYEY $AFTPY $CRWD $DAO $DDOG $DKNG $DOCU $FUBO $LSPD $MELI $OKTA $PINS $PLTR $ROKU $SE $SHOP $SNAP $SNOW $SQ $STNE $TWLO $U $UBER $VRM
```

Later on, they'll publish a new version of their portfolio:

```
$ADYEY $AFTPY $CRWD $DKNG $DOCU $FEAC $FUBO $LSPD $MELI $OKTA $OZON $PINS $PLTR $ROKU $SE $SHOP $SNAP $SQ $STNE $TDOC $TWLO $UBER $VRM $YNDX
```

If you're a layperson following these investors, can you easily tell what the changes are between the old and new portfolio? It's hard for humans to do but easy for computers.

## Solution

This script calculates the difference between old and new portfolios and shows you what was added, removed, and unchanged so you can update your own portfolio, with the output being:

```json
{
  added: [ 'FEAC', 'OZON', 'TDOC', 'YNDX' ],
  removed: [ 'DAO', 'DDOG', 'SNOW', 'U' ],
  unchanged: [
    'ADYEY', 'AFTPY', 'CRWD',
    'DKNG',  'DOCU',  'FUBO',
    'LSPD',  'MELI',  'OKTA',
    'PINS',  'PLTR',  'ROKU',
    'SE',    'SHOP',  'SNAP',
    'SQ',    'STNE',  'TWLO',
    'UBER',  'VRM'
  ]
}
```
## Instructions

1. Add or change `stocks/old.txt` with the old stock list. It can be in the format `$A $B $C` or (without `$`) `A B C` all on one line, or there can also be one stock on each new line.
2. Add or change `stocks/new.txt` with the new stock list, following the same format as the old stock list.
3. Install dependencies if you haven't already:
   1. `yarn set version berry`
   2. `yarn install`
4. Run `start.sh`. Alternatively, you can run `ts-node index.ts`, or `tsc index.ts && node index.js` if you want to run `index.ts` directly.