"use strict";
exports.__esModule = true;
var fs_1 = require("fs");
var processStocks = function (stocks) {
    return stocks
        .trim()
        .split(" ")
        .map(function (stock) { return stock.slice(1); })
        .sort();
};
var difference = function (oldStocks, newStocks) {
    var olds = processStocks(oldStocks);
    var news = processStocks(newStocks);
    return {
        // What's in the new list that's not in the old list
        added: news.filter(function (stock) { return !olds.includes(stock); }),
        // What's in the old list that's not in the new list
        removed: olds.filter(function (stock) { return !news.includes(stock); }),
        // What's in both the old and new lists
        unchanged: olds.filter(function (stock) { return news.includes(stock); })
    };
};
var oldStocks = fs_1.readFileSync("./stocks/old.txt").toString();
var newStocks = fs_1.readFileSync("./stocks/new.txt").toString();
console.log(difference(oldStocks, newStocks));
