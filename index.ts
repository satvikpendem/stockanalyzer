import { readFileSync } from "fs";

type Stock = string;
type StockListString = string;
type StockList = Stock[];

interface Results {
  added: StockList;
  removed: StockList;
  unchanged: StockList;
}

const processStocks = (stocks: StockListString): StockList =>
  stocks
    .trim()
    .split(/[\n\r\s]+/)
    .map((stock) => stock.replace("$", ""))
    .sort();

const difference = (
  oldStocks: StockListString,
  newStocks: StockListString
): Results => {
  const olds = processStocks(oldStocks);
  const news = processStocks(newStocks);

  return {
    // What's in the new list that's not in the old list
    added: news.filter((stock) => !olds.includes(stock)),
    // What's in the old list that's not in the new list
    removed: olds.filter((stock) => !news.includes(stock)),
    // What's in both the old and new lists
    unchanged: olds.filter((stock) => news.includes(stock)),
  };
};

const oldStocks = readFileSync("./stocks/old.txt").toString();
const newStocks = readFileSync("./stocks/new.txt").toString();
console.log(difference(oldStocks, newStocks));
